const { ApolloServer, gql } = require('apollo-server');
const { prisma } = require('./generated/prisma-client');

const typeDefs = gql`
  input CreateRampInput {
    achivementId: ID!
    type: String!
    x: Float
    y: Float
  }
  
  input CreateRampWithAchievementInput {
    type: String!
    x: Float
    y: Float
  }
  
  type Query {
    session(sessionId: ID!): Session
    sessions: [Session]
    achievements: [Achievement]
    ramps: [Ramp]
  }

  type Mutation {
    startSession: Session
    
    addAchievement(
      sessionId: ID!, 
      type: String,
      editTime: Float,
      playTime: Float,
      editCount: Int,
      playCount: Int,
      ramps: [CreateRampWithAchievementInput],
    ): Achievement
    
    addRamp(
      achievementId: ID!,
      type: String!
      x: Float
      y: Float
    ): Ramp
    
    addRamps(input: [CreateRampInput]): [Ramp]
  }
  

  type Session {
    id: ID!
    achievements: [Achievement!]
    createdAt: String,
    updatedAt: String,
  }

  type Achievement {
    id: ID!
    type: String
    session: Session!
    editTimeMinutes: Float
    playTimeMinutes: Float
    editModeCount: Int
    playModeCount: Int
    ramps: [Ramp!]
  }

  type Ramp {
    id: ID! 
    achievement: Achievement!
    type: String!
    x: Float
    y: Float
  }
`;

const resolvers = {
  Query: {
    session: (parent, { sessionId }) => prisma.session({ id: sessionId }),
    sessions: () => prisma.sessions({ where: { achievements_some: {} }}),
    achievements: () => prisma.achievements(),
    ramps: () => prisma.ramps(),
  },
  Mutation: {
    addAchievement:(parent, {
      sessionId,
      type,
      editTime,
      playTime,
      editCount,
      playCount,
      ramps,
    }) => {
      // console.log('_______________________________________');
      // console.log({ sessionId, type, editCount, editTime, playCount, playTime, ramps });
      return prisma.createAchievement({
        editTimeMinutes: editTime,
        playTimeMinutes: playTime,
        editModeCount: editCount,
        playModeCount: playCount,
        type,
        session: {
          connect: { id: sessionId },
        },
        ramps: {
          create: ramps,
        }
      });
    },
    startSession: () => prisma.createSession({}),
    addRamp: (parent, { achievementId, type, x, y, }) => prisma.createRamp({
      type,
      x,
      y,
      achievement: {
        connect: {id: achievementId}
      }
    }),
  },
  Session: {
    achievements(parent) {
      return prisma.session({ id: parent.id }).achievements();
    }
  },
  Achievement: {
    ramps(parent) {
      return prisma.achievement({ id: parent.id }).ramps();
    }
  }
};

const server = new ApolloServer({ typeDefs, resolvers });
server.listen().then(({ url }) => {
  console.log(`🚀  Server ready at ${url}`);
});
