import React, { useContext } from "react";
import { format } from 'date-fns';
import { PlaySessions } from '../contexts/play-sessions';

const formatTitle = play => {
  const time = play.createdAt;
  const dateFormat = 'MM/dd: hh:mm:ss';
  return format(new Date(time), dateFormat);
}

export default () => {
  const { sessions, setActiveSession, loading } = useContext(PlaySessions);

  if (loading) return null;

  return (
    <div>
      <h3>Select Session(s)</h3>
      <form>
        {
          <select onChange={event => setActiveSession(event.target.value)} defaultValue="1">
            <option value="all">All</option>
            {sessions.map(play => (
              <option value={play.id} key={play.id}>
                Play Session: {formatTitle(play)}
              </option>
            ))}
          </select>
        }
      </form>
    </div>
  );
};
