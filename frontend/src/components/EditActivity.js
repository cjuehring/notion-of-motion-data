import React from 'react';
import * as d3 from 'd3';

const styles = {
    activity: {
        margin: '20px',
        boxShadow: 'rgba(0,0,0, 0.3) 1px 1px 3px'
    },
    line: {
        stroke: 'rgb(0,0,0)',
        strokeWidth: '0.5',
    }
};

export const EditActivity = ({ editTime, playTime, edits }) => {
    const scaleTimeToY = d3.scaleLinear()
        .domain([0, Math.max(editTime, playTime)])
        .range([85, 5]);

    const scaleEditsToAxis = d3.scaleLinear()
      .domain([0, 20])
      .range([85, 5]);

    return (
      <svg style={styles.activity} width="250" height="250" viewBox="0 0 100 100">
          <rect fill="#fafafa" x="0" y="0" height="100" width="100" />

          <rect x="10" y={scaleTimeToY(editTime)} width="24" height="100" fill="hsl(40, 50%, 50%)" />
          <rect x="40" y={scaleTimeToY(playTime)} width="24" height="100" fill="hsl(80, 50%, 50%)" />
          <rect x="70" y={scaleEditsToAxis(edits)} width="24" height="100" fill="hsl(150, 50%, 50%)" />

          <line style={styles.line} x1="5" y1="5" x2="5" y2="85" />
          <line style={styles.line} x1="5" y1="85" x2="95" y2="85" />

          <rect x="5" y="85" width="90" height="30" fill="#fafafa" />

          <text x="13" y="92" fontSize="4">Edit Time</text>
          <text x="43" y="92" fontSize="4">Play Time</text>
          <text x="73" y="92" fontSize="4">Edits</text>

          <text x="13" y="98" fontSize="4">{ editTime }</text>
          <text x="43" y="98" fontSize="4">{ playTime }</text>
          <text x="73" y="98" fontSize="4">{ edits }</text>
      </svg>
    )
};

export default EditActivity;
