import _ from 'lodash';
import React from 'react';
import "./TimelineTable.css";

export default ({ session }) => {
  const startTime = new Date(session.createdAt);
  const achievements = _.get(session, 'achievements', []);

  return (
    <div className="timelinetable__container">
      <table className="timelinetable">
        <thead>
          <tr>
            <th>Achievement</th>
            <th>Elapsed Time</th>
            <th>Edit Time</th>
            <th>Play Time</th>
            <th>Play Mode Runs</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Session Start</td>
            <td>0.00</td>
            <td>0.00</td>
            <td>0.00</td>
            <td>0</td>
          </tr>
          {
            achievements.map(session => {
              return (
                <tr key={session.id}>
                  <td>{_.startCase(_.lowerCase(session.type))}</td>
                  <td>{_.round(session.editTimeMinutes + session.playTimeMinutes, 2)}</td>
                  <td>{_.round(session.editTimeMinutes, 2)}</td>
                  <td>{_.round(session.playTimeMinutes, 2)}</td>
                  <td>{_.round(session.playModeCount, 2)}</td>
                </tr>
              );
            })
          }
        </tbody>
      </table>
    </div>
  )
}
