import React from "react";
import * as d3 from 'd3';

const styles = {
  activity: {
    margin: "20px",
    boxShadow: 'rgba(0,0,0, 0.3) 1px 1px 3px'
  },
  line: {
    stroke: "rgb(0,0,0)",
    strokeWidth: "0.5"
  }
};

const mapXToScreen = d3.scaleLinear().domain([-25.47, 27.12]).range([0, 200]);
const mapYToScreen = d3.scaleLinear().domain([13.718, -6.57]).range([100, 0]);

export const RampPlot = ({ rampPositions }) => {
  const scaledLocationsRamps = rampPositions.map(({ramp, x, y})=> {
    return {
      ...ramp,
      x: mapXToScreen(x),
      y: mapYToScreen(y),
    }
  });

  return <svg style={styles.activity} width="450" height="250" viewBox="0 0 200 100">
    <rect fill="#fafafa" x="0" y="0" height="100" width="200" />
    <line style={styles.line} x1="5" y1="85" x2="190" y2="85" />
    {scaledLocationsRamps.map(ramp => (
      <circle key={`${ramp.x}-${ramp.y}`} cx={ramp.x} cy={85 - ramp.y} r="2" fill="rgba(0,0,0, 0.5)" />
    ))}
  </svg>
};

export default RampPlot;
