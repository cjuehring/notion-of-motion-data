import _ from 'lodash';
import React, { useContext } from 'react';
import { EditActivity } from './EditActivity';
import { RampPlot } from './RampPlot';
import { PlaySessions } from '../contexts/play-sessions';
import TimelineTable from './TimelineTable';

export const RampVisualization = ({ achievements, hideNumbers }) => {
  return achievements.map(achievement => {
    const { id, editTimeMinutes: editTime, playTimeMinutes: playTime, type, ramps, editModeCount } = achievement;

    return (
      <div key={ id }>
        <h4>{ _.startCase(_.toLower(type)) }</h4>
        {
          !hideNumbers ?
            <EditActivity editTime={ editTime } playTime={ playTime } edits={ editModeCount } /> :
            null
        }
        <RampPlot rampPositions={ ramps } />
      </div>
    )
  })
}

export const AllRampsVisualization = ({ sessions }) => {
  const sessionValues = _.take(_.values(_.reverse(sessions)), 100);
  const achievements = _.flatten(_.map(sessionValues, 'achievements', []));
  const groupedAchievements = _.groupBy(achievements, 'type');
  const groupedAchievementsWithFlatRamps = _.mapValues(groupedAchievements, (val, key) => {
    return { ramps: _.flatten(_.map(val, 'ramps')), type: key, id: key };
  });
  const achievementValues = _.values(groupedAchievementsWithFlatRamps);

  return (
    <RampVisualization key='all' achievements={achievementValues} hideNumbers={true} />
  );
};

export const ActivityView = () => {
  const { activeSession, sessions, loading } = useContext(PlaySessions);

    if (activeSession === 'all') {
      return (
        <AllRampsVisualization sessions={sessions} />
      );
    }

    const activeSessionRecord = _.find(sessions, { id: activeSession }, null);

    if (!activeSessionRecord || loading) return null;

    const achievements = _.get(activeSessionRecord, 'achievements');

    return (
      <div>
        <TimelineTable session={activeSessionRecord} />
        <RampVisualization achievements={achievements} />
      </div>
    );
};

export default ActivityView;
