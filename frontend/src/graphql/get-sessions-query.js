import gql from 'graphql-tag';

export const GET_SESSIONS_QUERY = gql`
    query {
        sessions {
            id
            updatedAt
            createdAt
            achievements {
                id
                playTimeMinutes
                editTimeMinutes
                playModeCount
                editModeCount
                type
                ramps {
                    id
                    type
                    x
                    y
                }
            }
        }
    }
`;
