import _ from 'lodash';
import React, { createContext, useState } from 'react';
import {useQuery} from '@apollo/react-hooks';
import { GET_SESSIONS_QUERY } from '../graphql/get-sessions-query';
import { client } from '../graphql/client';

export const PlaySessions = createContext(null);

export const PlaySessionsProvider = ({ children }) => {
  const [activeSession, setActiveSession] = useState(null);
  const { data, loading } = useQuery(GET_SESSIONS_QUERY, { client });

  const api = { sessions: _.get(data, 'sessions', {}), setActiveSession, activeSession, loading };

  return <PlaySessions.Provider value={ api }>{ children }</PlaySessions.Provider>;
};
