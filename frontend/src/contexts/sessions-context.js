import React, { createContext, useState } from "react";
import _ from "lodash";

export const SessionsContext = createContext(null);

const getRampPosition = () => _.range(3 + _.random(1, 10)).map((n, index) => ({
    type: 'rampType',
    x: (index * 25) + 5 + (_.random(0, 5)),
    y: _.random(5, 80 - (index * 5)),
}));

const makeTestSession = () => ({
  GoReallyFast: {
    editTime: _.random(5, 90),
    playTime: _.random(5, 90),
    rampPositions: getRampPosition()
  },
  GetSomeAir: {
    editTime: _.random(5, 90),
    playTime: _.random(5, 90),
    rampPositions: getRampPosition()
  },
  RampChallenge1: {
    editTime: _.random(5, 90),
    playTime: _.random(5, 90),
    rampPositions: getRampPosition()
  },
  RampChallenge2: {
    editTime: _.random(5, 90),
    playTime: _.random(5, 90),
    rampPositions: getRampPosition()
  },
  RampChallenge3: {
    editTime: _.random(5, 90),
    playTime: _.random(5, 90),
    rampPositions: getRampPosition()
  }
});

const testSessions = {
  activeSession: "1",
  playSessions: _.keyBy(_.range(1, 20).map(n => ({ id: n, ...makeTestSession() })), 'id')
};

const getActiveSession = (sessions) => {
  const { activeSession, playSessions } = sessions;
  if (activeSession === 'all') {

    const getAverage = (key) => {
      return _.map(playSessions, key).reduce((a, b) => a + b) / _.keys(playSessions).length;
    };

    const getRamps = (key) => {
      return _.flatten(_.map(playSessions, key))
    };

    return {
      GoReallyFast: {
        editTime: getAverage('GoReallyFast.editTime'),
        playTime: getAverage('GoReallyFast.playTime'),
        rampPositions: getRamps('GoReallyFast.rampPositions'),
      },
      GetSomeAir: {
        editTime: getAverage('GetSomeAir.editTime'),
        playTime: getAverage('GetSomeAir.playTime'),
        rampPositions: getRamps('GetSomeAir.rampPositions'),
      },
      RampChallenge1: {
        editTime: getAverage('RampChallenge1.editTime'),
        playTime: getAverage('RampChallenge1.playTime'),
        rampPositions: getRamps('RampChallenge1.rampPositions'),
      },
      RampChallenge2: {
        editTime: getAverage('RampChallenge2.editTime'),
        playTime: getAverage('RampChallenge2.playTime'),
        rampPositions: getRamps('RampChallenge2.rampPositions'),
      },
      RampChallenge3: {
        editTime: getAverage('RampChallenge3.editTime'),
        playTime: getAverage('RampChallenge3.playTime'),
        rampPositions: getRamps('RampChallenge3.rampPositions'),
      },
    }
  }

  return sessions.playSessions[activeSession];
};

export const SessionsContextProvider = ({ children }) => {
  const [sessions, setSessions] = useState(testSessions);
  const activeSession = getActiveSession(sessions);

  const setActiveSession = sessionId => {
    setSessions({ ...sessions, activeSession: sessionId });
  };

  return (
    <SessionsContext.Provider value={{ sessions, setActiveSession, activeSession }}>
      {children}
    </SessionsContext.Provider>
  );
};
