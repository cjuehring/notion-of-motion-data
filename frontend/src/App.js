import React from "react";
import { ApolloProvider } from 'react-apollo';
import "./App.css";
import SessionSelect from "./components/SessionSelector";
import ActivityView from "./components/ActivityView";
import { SessionsContextProvider } from "./contexts/sessions-context";
import { client } from './graphql/client';
import { PlaySessionsProvider } from './contexts/play-sessions';
import TimelineTable from './components/TimelineTable';
// import EditActivity from "./components/EditActivity";
// import RampPlot from "./components/RampPlot";

function App() {
  return (
    <SessionsContextProvider>
      <ApolloProvider client={client}>
        <PlaySessionsProvider>
          <div className="App">
            <SessionSelect />
            <hr />
            <ActivityView />
          </div>
        </PlaySessionsProvider>
      </ApolloProvider>
    </SessionsContextProvider>
  );
}

export default App;
